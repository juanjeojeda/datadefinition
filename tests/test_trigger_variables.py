# Copyright (c) 2020 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Gitlab api interaction tests."""
import unittest

from rcdefinition.trigger_variables import TriggerVariables


class TestTriggerVariables(unittest.TestCase):
    """Test TriggerVariables class."""

    def test_update(self):
        """Ensure trigger_variables update() works."""

        trigger_vars = TriggerVariables({})
        self.assertEqual(trigger_vars.skip_beaker, None)

        trigger_vars.update({'skip_beaker': True})

        self.assertEqual(trigger_vars.skip_beaker, True)
