# Copyright (c) 2020 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Gitlab api interaction tests."""
import unittest

from kcidb.oo import from_io

from kcidb_wrap import v3


class TestKCIDBWrap(unittest.TestCase):
    """Test KCIDBObject class."""

    @classmethod
    def test_craft(cls):
        """Ensure crafting and serialization works."""

        raw_data = {
            'revision_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
            'id': 'redhat:1',
            'origin': 'redhat',
            'description': 'data data',
            'start_time': '2019-11-14T00:07:00Z',
            'duration': 321,
            'architecture': 'x86_64',
            'command': 'make bzImage',
            'compiler': 'foo 1.2',
            'input_files': [],
            'output_files': [
                {'name': 'kernel.tar.gz',
                 'url': 'http://s3.server/kernel.tar.gz'}],
            'config_name': 'fedora',
            'config_url': 'http://s3.server/kernel.config',
            'log_url': 'http://s3.server/testfile.log',

            'misc': {'job_id': 3, 'pipeline_id': 3},
            'valid': True,
        }

        build = v3.Build(raw_data).to_mapping()
        schema = v3.craft_schema([], [build], [])

        from_io(schema)
