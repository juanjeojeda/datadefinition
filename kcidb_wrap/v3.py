#!/usr/bin/env python3
from rcdefinition.rc_data import DefinitionBase, _NO_DEFAULT

from kcidb.io.schema.v3 import JSON_TEST, JSON_REVISION, JSON_BUILD, JSON, \
    JSON_VERSION_MAJOR, JSON_VERSION_MINOR


def craft_schema(revisions, builds, tests):
    """Craft kcidb schema from internal data."""
    craft = {"version": dict(major=JSON_VERSION_MAJOR,
                             minor=JSON_VERSION_MINOR),
             "revisions": revisions, "builds": builds, "tests": tests}
    return craft


class KCIDBObject(DefinitionBase):
    """KCIDB v3 schema (de)serialization."""
    # dict object that describes metadata
    meta = JSON
    # type conversion table
    _conv_table = {'object': dict,
                   'string': str,
                   'number': int,
                   'array': list,
                   'boolean': bool}

    def __init__(self, dict_data=None):
        self.from_meta(self.meta)
        super(KCIDBObject, self).__init__(dict_data)

    def from_meta(self, schema):
        """Modify annotations to contain keys/types according to metadata."""
        self.__annotations__ = {}
        properties = schema['properties']
        required = schema['required']

        for key, value in properties.items():
            actual_type = KCIDBObject._conv_table[value['type']]
            self.__annotations__[key] = actual_type
            if key in required:
                setattr(self, key, _NO_DEFAULT)


class Revision(KCIDBObject):
    """KCIDB v3 Revision object."""
    meta = JSON_REVISION


class Build(KCIDBObject):
    """KCIDB v3 Build object."""
    meta = JSON_BUILD


class Test(KCIDBObject):
    """KCIDB v3 Test object."""
    meta = JSON_TEST
